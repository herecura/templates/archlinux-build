FROM docker.io/archlinux/archlinux:base as builder

RUN pacman-key --init \
    && pacman -Sy --noconfirm archlinux-keyring \
    && pacman -Syu --noconfirm devtools
RUN mkdir /archlinuxbuild \
    && mkdir -m 0755 -p \
        /archlinuxbuild/var/{cache/pacman/pkg,lib/pacman,log} \
        /archlinuxbuild/{dev,run,etc/pacman.d} \
    && mkdir -m 1777 -p /archlinuxbuild/tmp \
    && mkdir -m 0555 -p /archlinuxbuild/{sys,proc} \
    && cp -a /etc/pacman.d/gnupg /archlinuxbuild/etc/pacman.d/
RUN pacman --root /archlinuxbuild --dbpath /archlinuxbuild/var/lib/pacman \
           --sync --refresh --sysupgrade --noconfirm base-devel
RUN cp -a /usr/share/devtools/makepkg.conf.d/x86_64.conf \
          /archlinuxbuild/etc/makepkg.conf \
    && cp -a /etc/pacman.conf /archlinuxbuild/etc/pacman.conf

COPY ./root/ /archlinuxbuild/

RUN chmod 0755 /archlinuxbuild/usr/{,local/{bin,share}}

FROM scratch

COPY --from=builder /archlinuxbuild/ /

RUN echo 'Server = https://geo.mirror.pkgbuild.com/$repo/os/$arch' \
        > /etc/pacman.d/mirrorlist \
    && echo 'Server = https://mirrors.kernel.org/archlinux/$repo/os/$arch' \
        >> /etc/pacman.d/mirrorlist \
    && printf '%s.UTF-8 UTF-8\n' C en_US > /etc/locale.gen \
    && locale-gen \
    && pacman-key --init \
    && groupadd -g 107 archbuild \
    && useradd -u 107 -g 107 -m -c "Arch Linux Build User" \
               -s /bin/bash archbuild \
    && echo "archbuild ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/archbuild \
    && chmod u=rw,og=r /etc/sudoers.d/archbuild

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/bin
ENV LANG=C.UTF-8

USER archbuild
WORKDIR /home/archbuild

CMD ["/bin/bash"]
