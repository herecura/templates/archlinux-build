docker.io/blackikeeagle/archlinux-build
=======================================

Docker image for package building for Arch Linux.

Default user `archbuild`. Has `base-devel` preinstalled.

use:

```
docker pull registry.gitlab.com/herecura/templates/archlinux-build
```

or

```
docker pull docker.io/blackikeeagle/archlinux-build
```
